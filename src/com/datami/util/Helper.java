package com.datami.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.logging.LogEntry;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

import bsh.Capabilities;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.Activity;
//import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.android.Connection;

public class Helper {
	int count = 1;

	public static void listselection(WebDriver driver, String b) {

		List<WebElement> list = driver.findElements(By.className("android.widget.TextView"));
		for (WebElement a : list) {
			String c = a.getText().trim();
			System.out.println(c);
			if (c.toLowerCase().contains(b.toLowerCase())) {
				a.click();
				break;
			}
		}
	}

	/**
	 * Function Used for scroll and wait until something is found Pass the By
	 * element , it will try to find the value for 10 scrolls and then stops
	 */
	public void scrollAndClick(AndroidDriver driver, By appilcationname) {

		implictWait(driver, 3);
		boolean result = isElementPresent(driver, appilcationname);
		while (count < 30 && result == false) {
			scroll("UP", driver);
			result = isElementPresent(driver, appilcationname);
			System.out.println("Found");
		}
		implictWait(driver, 20);
	}

	/**
	 * Set implicit wait in seconds *
	 */
	public static void implictWait(AndroidDriver driver, int seconds) {
		driver.manage().timeouts().implicitlyWait(seconds, TimeUnit.SECONDS);

	}

	/**
	 * Function Used for waiting of a element Function to open notification and
	 * check for the required Text
	 */

	public boolean notificationHandle(AndroidDriver driver, String notificationtext) {
		// Notification handling
		int status = 0;
		driver.openNotifications();
		threadWait(2000);
		By notifications = By.xpath("//android.widget.TextView[contains(@resource-id,'android:id/text')]");
		List<WebElement> lis = driver.findElements(notifications);
		// System.out.println(lis.size());
		for (WebElement a : lis) {
			// System.out.println(a.getText());
			if (a.getText().trim().contains(notificationtext)) {
				status = 1;
				screenShots(driver, notificationtext + " is shown");
				Reporter.log(a.toString());
				break;
			}
		}
		if (status == 1) {
			System.out.println("The Notification is shown and screenshot taken");
			driver.pressKeyCode(AndroidKeyCode.BACK);
			return true;
		} else {
			System.out.println("**********************NO NOTIFICATIONS********************");
			driver.pressKeyCode(AndroidKeyCode.BACK);
			return false;
		}
	}

	/**
	 * Function Used for waiting
	 */

	public static boolean loadedView(AndroidDriver driver, By view) {
		int a = 1;
		WebDriverWait wait = new WebDriverWait(driver, 40);
		String view1 = driver.findElement(view).getText();
		try {
			System.out.println("Inside Try Block");
			wait.until(ExpectedConditions.visibilityOf(driver.findElement(view)));

		} catch (Exception e) {
			System.out.println("Not Found the element" + " :" + view1);
			a = 0;
		}
		if (a == 0) {
			return true;
		} else
			return false;
	}

	/**
	 * Function Used for checking if element is clickable or not Pass element BY
	 */

	private boolean isElementPresent(AndroidDriver driver, By a) {
		try {
			driver.findElement(a);
			driver.findElement(a).click();
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	/**
	 * Function Used for scroll Pass driver and direction "DOWN" // "UP"
	 */

	public void scroll(String direction, AndroidDriver driver) {
		TouchAction touchAction = new TouchAction(driver);
		// Helpers.driver.context("NATIVE_APP");
		Dimension size = driver.manage().window().getSize();
		int startx = 0;
		int starty = 0;
		int endx = 0;
		int endy = 0;
		switch (direction) {
		case "DOWN":
			startx = (int) (size.width * 0.5);
			endx = startx;
			starty = size.height / 3; // 400
			endy = size.height / 2; // 600
			System.out.println("SCROLLLED DOWN");
			break;

		case "UP":
			startx = (int) (size.width * 0.5);
			endx = startx;
			starty = (int) (size.height / 1.31); // 600
			endy = size.height / 8; // 300
			System.out.println("SCROLLLED UP");
			break;
		}

		touchAction.longPress(startx, starty).moveTo(endx, endy).release().perform();
		// driver.swipe(startx, starty, endx, endy, 1000);

	}

	/**
	 * Function Used for swipping It Swipes for any device Need to Specify the
	 * Device Direction
	 */

	public void swipe(AndroidDriver driver, String direction) {
		TouchAction touchAction = new TouchAction(driver);
		threadWait(30);
		Dimension size = driver.manage().window().getSize();
		System.out.println(size);
		int startx = 0, endx = 0, starty = 0, endy = 0;

		switch (direction.toLowerCase()) {
		case "right":
			System.out.println("Here at Right");
			startx = (int) (size.width * 0.01);
			endx = (int) (size.width * 0.8);
			starty = size.height / 2;
			endy = size.height / 2;
			break;

		case "left":
			startx = (int) (size.width * 0.8);
			endx = (int) (size.width * 0.01);
			starty = size.height / 2;

			break;

		case "up":
			startx = (int) (size.width * 0.5);
			endx = startx;
			starty = (int) (size.height / 1.31);
			endy = size.height / 8;

			break;

		case "down":
			startx = size.width / 2;
			endx = size.width / 2;
			starty = (int) (size.height * 0.6);
			endy = (int) (size.height * 0.1);

			break;

		}
		touchAction.longPress(startx, starty).moveTo(endx, endy).release().perform();
		// driver.swipe(startx, starty, endx, endy, 2000);
		threadWait(1);
	}

	/**
	 * Function Used to hard wait.. In miliseconds
	 ** 
	 */

	public void threadWait(int time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Function Used to Diable Wifi if ON Currently working for Samsung (6.0 only)
	 * Storing the value in the Screenshot file
	 */

	public void wifiControl(AndroidDriver driver, String device, String status) {

		By wifiChk = By.xpath("//android.widget.Switch[contains(@index,'1')]");
		if (device.equalsIgnoreCase("SAM")) {

			activitySwitch(driver, "com.android.settings", "com.android.settings.Settings");
			threadWait(5000);
			System.out.println("Waiting for Wifi");
			listselection(driver, "Wi-Fi");
			threadWait(2000);
			if (status.contains("OFF")) {
				if (driver.findElement(wifiChk).getText().equalsIgnoreCase("ON")) {
					driver.findElement(wifiChk).click();
				}
			}

			if (status.contains("ON")) {

				if (driver.findElement(wifiChk).getText().equalsIgnoreCase("ON")) {
					System.out.println("Already ON");
				} else {
					driver.findElement(wifiChk).click();
				}
			}
			driver.pressKeyCode(AndroidKeyCode.BACK);
			activitySwitch(driver, "acr.browser.lightning", "acr.browser.lightning.MainActivity");

		}
	}

	/**
	 * Function Used for startLogcatFile Storing the value in the Screenshot file
	 */

	public static void startLogcatFile(AndroidDriver driver) throws FileNotFoundException {
		{
			DateFormat df = new SimpleDateFormat("dd_MM_yyyy_HH-mm-ss");
			Date today = Calendar.getInstance().getTime();
			String reportDate = df.format(today);
			String logPath = "./Screenshots/";
			// log.info(driver.getSessionId() + ": Saving device log...");
			List<LogEntry> logEntries = driver.manage().logs().get("logcat").filter(Level.ALL);
			File logFile = new File(logPath + reportDate + "_" + ".log");
			PrintWriter log_file_writer = new PrintWriter(logFile);
			log_file_writer.println(logEntries);
			log_file_writer.flush();

			// log.info(driver.getSessionId() + ": Saving device log - Done.");

		}
	}

	/**
	 * Function Used for Taking Screenshot
	 */
	public static void screenShots(AndroidDriver driver, String a) {

		try {
			TakesScreenshot s = (TakesScreenshot) driver;
			String filename = "./Screenshots/" + a;
			String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
			File Filepath = new File(filename + " " + timeStamp + ".png");
			FileUtils.copyFile(s.getScreenshotAs(OutputType.FILE), Filepath);
			String screenshot = Filepath.toString();
			String path = ("<br> <img src=" + screenshot + " /> <br>");

			Reporter.log(path);
			System.out.println("ScreenShot Taken : " + filename);

		} catch (Exception e) {
			Reporter.log(e.getLocalizedMessage());
		}

	}

	// Setting App reading
	By cellularServiceProvider = By.name("Vodafone IN");
	By datausages = By.xpath("//android.widget.TextView[@text='Data usage']");

	public void clickOnDataUsages(AndroidDriver driver) {

		activitySwitch(driver, "com.android.settings", "com.android.settings.Settings");

		String version = driver.getCapabilities().getCapability("platformVersion").toString();
		if (version.contains("6.0.1") || version.contains("5.1.1")) {
			listselection(driver, "Data usage");
		}
		if (version.contains("8.0")) {
			listselection(driver, "Network & Internet");
			threadWait(3000);
			listselection(driver, "Data usage");
			threadWait(5000);
			listselection(driver, "Mobile data usage");
		}
		if (version.contains("7.0")) {
			listselection(driver, "Data usage");
			listselection(driver, "Cellular data usage");
		}
	}

	public void dataProvider(AndroidDriver driver) {

		driver.findElement(cellularServiceProvider).click();

	}

	public void findAppInSetting(AndroidDriver driver, By appilcationname) {
		scrollAndClick(driver, appilcationname);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// Function to change to flight mode.
	// Works in Samsung devices

	public void flightMode(AndroidDriver driver) {
		driver.setConnection(Connection.AIRPLANE);

	}

	// Activity Change
	// Accepts 2 parameters || settingsAppPackageName || settingsAppActivityName

	public void activitySwitch(AndroidDriver driver, String AppPackageName, String AppActivityName) {

		Activity activity = new Activity(AppPackageName, AppActivityName);
		activity.setStopApp(false);
		((AndroidDriver) driver).startActivity(activity);

	}

	// Function to go to setting and capture the data usages.
	// Works in Samsung devices

	public float dataCollection(AndroidDriver driver) {

		String version = driver.getCapabilities().getCapability("platformVersion").toString();

		By dataSummary = null;
		By dataForeground = null;
		By dataBackground = null;
		String strdataTotal = null;
		String strdataBackground = null;
		String strdataForeground = null;

		if (version.contains("7.0") || version.contains("8.0")) {

			dataSummary = By.id("android:id/summary");
			List<WebElement> list = driver.findElements(dataSummary);
			for (int i = 0; i < list.size(); i++) {

				strdataTotal = list.get(0).getText();
				strdataForeground = list.get(1).getText();
				strdataBackground = list.get(2).getText();

			}
		}

		if (version.contains("6.0.1") || version.contains("5.1.1")) {
			dataSummary = By.id("com.android.settings:id/app_summary");
			dataForeground = By.id("com.android.settings:id/app_foreground");
			dataBackground = By.id("com.android.settings:id/app_background");

			strdataTotal = driver.findElement(dataSummary).getText();
			strdataBackground = driver.findElement(dataBackground).getText();
			strdataForeground = driver.findElement(dataForeground).getText();
		}
		Float.valueOf(strdataTotal);
		screenShots(driver, "Data Screen");
		Reporter.log("Total Data Consumed :" + strdataTotal);
		Reporter.log("Background Data Consumed :" + strdataBackground);
		Reporter.log("Foreground Data Consumed :" + strdataForeground);

		
		System.out.println(strdataTotal + "  " + strdataBackground + "  " + strdataForeground + "   ");
		return Float.valueOf(strdataTotal);
	}

	public void wifiApp(AndroidDriver driver, String status) {

		activitySwitch(driver, "com.connection.myapplication", "com.connection.myapplication.MainActivity");
		By wifiChk = By
				.xpath("//android.widget.Switch[contains(@resource-id,'com.connection.myapplication:id/switch_wifi')]");
		driver.findElement(wifiChk).click();

		threadWait(2000);

		if (status.toUpperCase().contains("OFF")) {
			if (driver.findElement(wifiChk).getText().equalsIgnoreCase("ON")) {
				driver.findElement(wifiChk).click();
				threadWait(3000);
			}
		}

		if (status.toUpperCase().contains("ON")) {

			if (driver.findElement(wifiChk).getText().equalsIgnoreCase("ON")) {
				System.out.println("Already ON");
			} else {
				driver.findElement(wifiChk).click();
			}
		}
		driver.pressKeyCode(AndroidKeyCode.BACK);
		activitySwitch(driver, "acr.browser.lightning", "acr.browser.lightning.MainActivity");

	}

	public void flightMode2(AndroidDriver driver, String status) {
		// TouchAction touchAction = new TouchAction(driver);
		String version = driver.getCapabilities().getCapability("platformVersion").toString();
		activitySwitch(driver, "com.android.settings", "com.android.settings.Settings");
		threadWait(1000);
		if (version.contains("6.0.1") || version.contains("5.1.1") || version.contains("7.0")) {
			listselection(driver, "More");
			List<WebElement> list = driver.findElements(By.className("android.widget.Switch"));
			for (WebElement a : list) {
				String c = a.getText().trim();
				System.out.println(c);
				if (status.toLowerCase().contains("on")) {
					if (c.contains("OFF")) {
						a.click();
						Reporter.log("Switched to Flight mode");
						break;
					}
				} else {
					if (c.contains("ON")) {
						a.click();
						break;
					}

				}
			}
		} else if (version.contains("8.0")) {
			listselection(driver, "Network & Internet");
			listselection(driver, "Airplane mode");
		}

		driver.pressKeyCode(AndroidKeyCode.BACK);
		threadWait(1000);
		activitySwitch(driver, "acr.browser.lightning", "acr.browser.lightning.MainActivity");
	}

	public void dualSimSupport(AndroidDriver driver, String service) {

		activitySwitch(driver, "com.android.settings", "com.android.settings.Settings");

		String version = driver.getCapabilities().getCapability("platformVersion").toString();
		if (version.contains("6.0.1") || version.contains("5.1.1") || version.contains("7.0")) {
			listselection(driver, "SIM cards");
			threadWait(2000);

			if (version.contains("7.0")) {
				scroll("UP", driver);
				listselection(driver, "Cellular data");

			} else {
				listselection(driver, "Mobile data");
			}

			threadWait(2000);
			listselection(driver, service);
			threadWait(2000);
			List<WebElement> list = driver.findElements(By.className("android.widget.Button"));
			for (WebElement a : list) {
				String c = a.getText().trim();
				System.out.println(c);
				if (c.contains("OK")) {
					a.click();
					break;
				}
			}
		}

		else if (version.contains("8.0")) {
			listselection(driver, "Network & Internet");
			threadWait(2000);
			listselection(driver, "SIM cards");
			threadWait(2000);
			listselection(driver, "Cellular data");
			threadWait(2000);
			listselection(driver, service);
			threadWait(2000);

		}

		driver.pressKeyCode(AndroidKeyCode.BACK);
		threadWait(20000);
		activitySwitch(driver, "acr.browser.lightning", "acr.browser.lightning.MainActivity");

	}

	/**
	 * Function reset the app app is reseted by the function
	 */
	public void reset(AndroidDriver driver) {
		driver.resetApp();
		threadWait(3000);
	}

	/**
	 * Android Function to load url. The function switches to Webview and then
	 * returns to app view Checks if the web is loaded and then logs the report
	 * accordingly
	 */
	public boolean browseData(AndroidDriver driver, String Url) {
		try {
			By searchBox = By
					.xpath("//android.widget.EditText[contains(@resource-id,'acr.browser.lightning:id/search')]");
			threadWait(2000);
			driver.context("NATIVE_APP");
			System.out.println(driver.isBrowser());
			driver.findElement(searchBox).sendKeys(Url);
			driver.pressKeyCode(AndroidKeyCode.ENTER);
			threadWait(10000);
			Set<String> contexts = driver.getContextHandles();
			System.out.println(driver.isBrowser());
			for (String s : contexts) {
				System.out.println(s);
				if (s.contains("WEBVIEW")) {
					System.out.println("Mobile Web View found");
					driver.context(s);
				}
			}
			String title = driver.getTitle();
			Reporter.log("The web page Title is" + driver.getTitle());
			if (title.contains("Web Page not available")) {
				Reporter.log("Unable to Load the Page.");
				return (false);
			} else {
				driver.context("NATIVE_APP");
				swipe(driver, "up");
				swipe(driver, "up");
				return (true);
			}

		} catch (Exception e) {
			System.out.println(e.getLocalizedMessage());
			return (false);
		}

	}
}
