package com.datami.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Scanner;

import org.testng.Reporter;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ReadText {

	public static Float dataInDas(String Deviceid, String Pkgid) {
	return	parseJson(Deviceid, Pkgid);
	}

	private static float parseJson(String Deviceid, String Pkgid) {
		String jsonStr = null;
		Date today = new Date();
		float total1=0;
		Date date = new Date();
		LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

		String setup = Pkgid.substring(0, Math.min(Pkgid.length(), 3));
		try {
			if (setup.toLowerCase().contains("qin")) {
				jsonStr = getUrlContents(
						"http://qin-api.cloudmi.datami.com/das/zmi-accounting/v0/package/" + Pkgid + "/user/" + Deviceid
								+ "/year/2018/month/" + localDate.getMonthValue() + "/day/" + today.getDate() + "");
			} else if (setup.toLowerCase().contains("stgin")) {
				jsonStr = getUrlContents("http://stgin-api.cloudmi.datami.com/das/zmi-accounting/v0/package/" + Pkgid
						+ "/user/" + Deviceid + "/year/2018/month/" + localDate.getMonthValue() + "/day/"
						+ today.getDate() + "");
			}

			else if (setup.toLowerCase().contains("demo")) {
				jsonStr = getUrlContents("http://test.ain-api.cloudmi.datami.com/das/zmi-accounting/v0/package/" + Pkgid
						+ "/user/" + Deviceid + "/year/2018/month/" + localDate.getMonthValue() + "/day/"
						+ today.getDate() + "");
			}
		} catch (Exception e) {
			e.getMessage();

		}

		if (jsonStr.isEmpty()) {
			float total =0;
			
		} else {
			JsonParser parser = new JsonParser();
			JsonObject mainObj = (JsonObject) parser.parse(jsonStr);
			JsonObject nspObj = mainObj.getAsJsonObject("sp");
			float total = nspObj.get("total").getAsLong();
			 total1 = total / 1024;
			System.out.println("total: " + total);
			Reporter.log("Total Data as Shown in DAS URL:" + total1 / 1024);
		}
	return total1;
	
	}
	

	private static String getUrlContents(String theUrl) {
		StringBuilder content = new StringBuilder();

		// many of these calls can throw exceptions, so i've just
		// wrapped them all in one try/catch statement.
		try {
			// create a url object
			URL url = new URL(theUrl);

			// create a urlconnection object
			URLConnection urlConnection = url.openConnection();

			// wrap the urlconnection in a bufferedreader
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

			String line;

			// read from the urlconnection via the bufferedreader
			while ((line = bufferedReader.readLine()) != null) {
				content.append(line + "\n");
			}
			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return content.toString();
	}
}
