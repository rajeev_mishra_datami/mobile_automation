package com.datami.util;

import java.io.File;
import java.io.FileInputStream;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {

	XSSFWorkbook wb;
	XSSFSheet Sheet1;

	public ExcelUtil(String filepath) {
		try {
			File src = new File(filepath);
			FileInputStream fis = new FileInputStream(src);
			wb = new XSSFWorkbook(fis);

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public String getData(int sheetNumber, int row, int column) {
		Sheet1 = wb.getSheetAt(sheetNumber);
		return Sheet1.getRow(row).getCell(column).getStringCellValue();
	}

	public String getDataNumeric(int sheetNumber, int row, int column) {
		Sheet1 = wb.getSheetAt(sheetNumber);
		return Sheet1.getRow(row).getCell(column).getRawValue();
	}

	public int getrowcount(int Sheetindex) {
		int row = wb.getSheetAt(Sheetindex).getLastRowNum();
		row = row + 1;
		return row;

	}

}
