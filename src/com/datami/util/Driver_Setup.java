package com.datami.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;

public class Driver_Setup {

	private AndroidDriver driver;
	String appiumServiceUrl;
	AppiumDriverLocalService appiumService;

	public AndroidDriver getDriver() {
		System.out.println("I am here getDriver");
		return driver;
	}

	private void setDriver(String apptype, String appName, String AndroidVersion) {
		appiumService = AppiumDriverLocalService.buildDefaultService();
		appiumService.start();
		appiumServiceUrl = appiumService.getUrl().toString();
		System.out.println("Appium Service Address : - " + appiumServiceUrl);
		DesiredCapabilities capabilities = new DesiredCapabilities();
		switch (apptype) {
		case ("NativeApp:My Moto"): {
			// capabilities.setCapability("Device", "Moto");
			capabilities.setCapability("appium-version", "1.7.2");

			System.out.println(System.getenv("ANDROID_HOME"));
			capabilities.setCapability("deviceName", "MOTO");
			capabilities.setCapability("platformVersion", AndroidVersion);
			capabilities.setCapability("platformName", "Android");
			capabilities.setCapability("autoDismissAlerts", true);
			capabilities.setCapability("newCommandTimeout", 220);
			capabilities.setCapability("unicodeKeyboard", false);
			try {
				// File file = new File("./Screenshots//LighteningBrowser_1.9.6_release.apk");
				File file = new File("./Application//" + appName);
				capabilities.setCapability("app", file.getAbsolutePath());
			} catch (Exception e) {
				System.out.println(e.getLocalizedMessage());
			}
			break;
		}
		}
		try {
			System.setProperty("org.uncommons.reportng.escape-output", "false");
			driver = new AndroidDriver(new URL(appiumServiceUrl), capabilities);
			try {
				Helper.startLogcatFile(driver);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}
	}

	@Parameters({ "apptype", "appName", "AndroidVersion" })
	@BeforeClass
	public void start(String apptype, String appName, String AndroidVersion) {
		System.out.println("I am here start");
		try {
			setDriver(apptype, appName, AndroidVersion);
		} catch (Exception e) {
			System.out.println("No values found:" + e.getMessage());
		}
	}

	@AfterClass
	public void teardown() {
		System.out.println("End of Program: Tear Down Called");
		driver.removeApp("acr.browser.lightning");
		driver.quit();
		appiumService.stop();
	}

	@AfterMethod
	public void teardown(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
			appiumService.stop();
			System.out.println("Out of here");
		}

	}
}
