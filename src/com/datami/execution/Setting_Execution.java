package com.datami.execution;


import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.datami.testcases.SettingApp;
import com.datami.util.Driver_Setup;

import io.appium.java_client.android.AndroidDriver;


public class Setting_Execution extends Driver_Setup{

	
	private AndroidDriver driver;
	private SettingApp objsetting = null;
	
	
	@BeforeClass
	public void setUp() {
		driver = getDriver();
		objsetting = new SettingApp(driver);
		
		
	}
	
	@Test(priority=1)
	public void clickOnDataUsages() {
		objsetting.clickOnDataUsages();
		objsetting.dataProvider();
		objsetting.findAppInSetting();
		objsetting.dataCollection();
	}
	
	
}
