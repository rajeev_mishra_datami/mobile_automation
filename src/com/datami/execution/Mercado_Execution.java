package com.datami.execution;

import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.datami.testcases.LightningApp;
import com.datami.testcases.MercadoApp4_3;
import com.datami.util.Driver_Setup;

import io.appium.java_client.android.AndroidDriver;

public class Mercado_Execution extends Driver_Setup {

	private AndroidDriver driver;
	private MercadoApp4_3 mercado = null;
	private LightningApp objlightning = null;

	@BeforeClass
	public void setUp() {

		driver = getDriver();
		mercado = new MercadoApp4_3(driver);
		objlightning = new LightningApp(driver);
	}

	@Test(priority = 1, alwaysRun = true)
	public void startTest() {
	
		mercado.start();
	}

	@Test(priority = 2, alwaysRun = true)
	public void login() {
		mercado.login();
	}

	@Test(priority = 3, alwaysRun = true)
	public void search() {
		mercado.searchPage();
	}

	@Test(priority = 4, alwaysRun = true)
	public void settingCall() {
		mercado.settingApp();
	}

}
