package com.datami.execution;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.datami.testcases.BackgroundTest;
import com.datami.testcases.LightningApp;

import com.datami.util.Driver_Setup;

import io.appium.java_client.android.AndroidDriver;

@Listeners(com.datami.listeners.Iexecutionlistener.class)

public class Background_Test extends Driver_Setup {
	BackgroundTest background = null;
	private AndroidDriver driver;
	private LightningApp objlightning = null;

	@BeforeClass
	public void setUp() {
		driver = getDriver();
		background = new BackgroundTest(driver);

	}

	@Test(priority = 1, alwaysRun = true)
	public void settingCall() {
		background.clickAndGo();
	}

	@Test(priority = 2, enabled = true)
	public void sdkPopup() {
		objlightning.sdkPopup();
	}
}
