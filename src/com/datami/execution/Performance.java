package com.datami.execution;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.datami.testcases.PerformanceTest;
import com.datami.util.Driver_Setup;

import io.appium.java_client.android.AndroidDriver;

@Listeners(com.datami.listeners.Iexecutionlistener.class)

public class Performance extends Driver_Setup {

	private AndroidDriver driver;
	private PerformanceTest perf = null;

	@BeforeClass
	public void setUp() {
		driver = getDriver();
		perf = new PerformanceTest(driver);

	}

	@Test(priority = 1, enabled = true)
	public void wifiOff() {
		perf.wifiOff();
	}

	@Test(priority = 2, enabled = true)
	public void sdkPopup() {
		perf.sdkPopup();

	}

	@Test(priority = 3, enabled = true)
	public void reset() {
		perf.reset();
	}

	@Test(priority = 4, enabled = true)
	public void sdkPopup2() {
		perf.sdkPopup();
	}

	@Test(priority = 5, enabled = true)
	public void reset2() {
		perf.reset();
	}

	@Test(priority = 6, enabled = true)
	public void sdkPopup4() {
		perf.sdkPopup();
	}

	@Test(priority = 7, enabled = true)
	public void reset3() {
		perf.reset();
	}

	@Test(priority = 8, enabled = true)
	public void sdkPopup5() {
		perf.sdkPopup();
	}
}
