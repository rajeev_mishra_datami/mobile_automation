package com.datami.execution;

import java.io.File;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.datami.testcases.LightningApp;

import com.datami.util.Driver_Setup;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.Connection;

@Listeners(com.datami.listeners.Iexecutionlistener.class)

public class LightningExecution extends Driver_Setup {

	private AndroidDriver driver;
	private LightningApp objlightning = null;
	float a = 0;
	float total = 0;

	@BeforeClass
	public void setUp() {
		driver = getDriver();
		objlightning = new LightningApp(driver);
	}

	@Parameters({ "userID", "pkgID" })
	@Test(priority = 1, enabled = true)
	public void sdkPopup(String userID, String pkgID) {

		a = objlightning.dataInDas(userID, pkgID);
		// objlightning.dualSimSupport("Airtel");
		objlightning.sdkPopup();
	}

	@Test(priority = 2, enabled = true)
	public void notification() {
		objlightning.notification();
	}

	@Test(priority = 3, enabled = true)
	public void browseData() {
		objlightning.browseData();
	}

	@Test(priority = 4, enabled = true)
	public void browseOnSecondTab() {
		objlightning.browseOnSecondTab();
	}

	@Test(priority = 5, enabled = true)
	public void wifiSearch() {
		objlightning.wifiSearch();
	}

	@Test(priority = 6, enabled = true)
	public void dualSimSupport() {
		objlightning.dualSimSupport("VODAFONE IN");

	}

	@Test(priority = 7, enabled = true)
	public void flightModeBrowser() {
		objlightning.flightModeBrowser();
	}

	@Parameters({ "userID", "pkgID" })
	@Test(priority = 8, alwaysRun = true)
	public void settingCall(String userID, String pkgID) {
		total = objlightning.dataUsed(userID, pkgID);

	}

	@Parameters({ "userID", "pkgID" })
	@Test(priority = 9, alwaysRun = true)
	public void dataAtLast(String userID, String pkgID) {
		Float b = objlightning.dataInDas(userID, pkgID);
		Float d = a - b;
		Reporter.log("Data Used:" + d);
		Float m = total - d;
		Reporter.log("Data Differnce" + m);

	}

}
