package com.datami.execution;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.datami.testcases.LightningApp;
import com.datami.testcases.MercadoApp4_3;
import com.datami.testcases.MercadoApp_8;
import com.datami.util.Driver_Setup;

import io.appium.java_client.android.AndroidDriver;

public class Mercado_Execution_8 extends Driver_Setup {

	private AndroidDriver driver;
	private MercadoApp_8 mercado = null;
	private LightningApp objlightning = null;

	@BeforeClass
	public void setUp() {

		driver = getDriver();
		mercado = new MercadoApp_8(driver);
		objlightning = new LightningApp(driver);
	}

	@Test(priority = 1, alwaysRun = true)
	public void startTest() {
		mercado.start();
	}

	@Test(priority = 2, alwaysRun = true)
	public void login() {
		mercado.login();
	}

	@Test(priority = 2, alwaysRun = true)
	public void search() {
		mercado.searchPage();
	}

	@Test(priority = 3, alwaysRun = true)
	public void settingCall() {
		mercado.settingApp();

	}

}
