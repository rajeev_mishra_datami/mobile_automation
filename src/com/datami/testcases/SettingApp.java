package com.datami.testcases;

import org.openqa.selenium.By;
import com.datami.util.Helper;
import io.appium.java_client.android.AndroidDriver;

public class SettingApp {
	
	AndroidDriver driver;
	
	Helper help = new Helper();
	public SettingApp(AndroidDriver driver) {
		this.driver = driver;
	}
	
	By datausages= By.xpath("//android.widget.Button[@text='Data usage']");//By.name("Data usage");
	By appilcationname= By.name("Birthdays");
	By cellularServiceProvider= By.name("Vodafone IN");
	By dataSummary=By.id("com.android.settings:id/app_summary");
	By dataBackground=By.id("com.android.settings:id/app_foreground");
	By dataForeground=By.id("com.android.settings:id/app_background");
	
	
	public void clickOnDataUsages() {
		
		driver.findElement(datausages).click();
		
	}

	public void dataProvider() {
		
	driver.findElement(cellularServiceProvider).click();
		
	}
	
	public void findAppInSetting() {
			help.scrollAndClick(driver,appilcationname);
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
		}
	
	public void dataCollection() {
		String strdataTotal=driver.findElement(dataSummary).getText();
		String strdataBackground=driver.findElement(dataBackground).getText();
		String strdataForeground=driver.findElement(dataForeground).getText();
		
		System.out.println(strdataTotal+"  "+strdataBackground+"  "+strdataForeground+"   ");
	}
	
	
	
	}
