package com.datami.testcases;

import org.openqa.selenium.By;

import com.datami.util.Helper;

//import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;

public class BackgroundTest {

	AndroidDriver driver;

	String settingsAppPackageName = "com.luizalabs.mlapp";
	String settingsAppActivityName = "com.luizalabs.mlapp.legacy.ui.activities.HomeActivity";
	By appilcationname = By.xpath("//android.widget.TextView[@text='Magazine Luiza']");

	By searchUrl = By
			.xpath("//android.widget.EditText[contains(@resource-id,'com.example.urlapitestapp:id/url_editor')]");

	By goButton = By.xpath("//android.widget.Button[contains(@resource-id,'com.example.urlapitestapp:id/go_button')]");
	Helper help = new Helper();
	// private SoftAssert softAssert = new SoftAssert();

	String pathofExcel = ".//Excel//Workbook1.xlsx";
	com.datami.util.ExcelUtil excel = new com.datami.util.ExcelUtil(pathofExcel);

	String id = excel.getData(0, 1, 1).trim();

	public BackgroundTest(AndroidDriver driver) {
		this.driver = driver;

	}

	public void changeToMagazine() throws InterruptedException {

		Thread.sleep(5000);
		System.out.println("Start Of Waiting");

		help.findAppInSetting(driver, appilcationname);

	}

	public void dataUsed() {
		help.clickOnDataUsages(driver);
		help.scrollAndClick(driver, appilcationname);
		help.threadWait(2000);
		help.dataCollection(driver);
	}

	public void clickAndGo() {
		for (int i = 0; i < 100; i++) {
			
			String url = excel.getData(0, i, 2).trim();
			driver.findElement(searchUrl).sendKeys(url);
			driver.findElement(goButton).click();
			driver.findElement(searchUrl).clear();
			help.screenShots(driver, url);

		}
	}
}
