package com.datami.testcases;

import org.openqa.selenium.By;

import com.datami.util.Helper;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

public class MercadoApp4_3 {

	AndroidDriver driver;

	Helper help = new Helper();

	public MercadoApp4_3(AndroidDriver driver) {
		this.driver = driver;
	}

	By upgradeLater = By.xpath("//android.widget.Button[contains(@resource-id,'android:id/button2')]");
	By layout = By.xpath("//android.widget.RelativeLayout[@index='2']");

	// Login
	By alreadyLoggedUser = By.xpath(
			"//android.widget.Button[contains(@resource-id,'com.mercadolibre:id/home_onboarding_already_has_account_button')]");
	By emailId = By
			.xpath("//android.widget.EditText[contains(@resource-id,'com.mercadolibre:id/login_user_edit_text')]");
	By password = By.xpath("//android.widget.EditText[@text='Senha']");
	By enterButton = By
			.xpath("//android.widget.Button[contains(@resource-id,'com.mercadolibre:id/login_sign_in_button')]");

	// SearchProduct

	By textSearchButton = By
			.xpath("//android.widget.TextView[contains(@resource-id,'com.mercadolibre:id/home_search')]");
	By textData = By
			.xpath("//android.widget.EditText[contains(@resource-id,'com.mercadolibre:id/search_input_edittext')]");
	By appilcationname = By.xpath("//android.widget.TextView[@text='Mercado Libre']");
	//AfterScroll
	By searchAfterScroll=By
			.xpath("//android.widget.TextView[contains(@resource-id,'com.mercadolibre:id/search_menu_search_item')]");
	
	public void start() {
		help.implictWait(driver, 40);
		driver.findElement(upgradeLater).click();

		driver.findElement(layout).click();
		help.threadWait(1000);
		help.swipe(driver, "Left");
		help.swipe(driver, "Left");
		help.swipe(driver, "Left");
		help.swipe(driver, "Left");
	}

	public void login() {
		driver.findElement(alreadyLoggedUser).click();
		driver.findElement(emailId).sendKeys("murarij@yahoo.com");
		driver.findElement(password).sendKeys("datami@123");
		driver.findElement(enterButton).click();
	}

	public void searchPage() {
		help.threadWait(5000);
		
		for (int i = 0; i < 20; i++) {
			help.threadWait(4000);
			help.scroll("UP", driver);
			help.scroll("UP", driver);
			help.scroll("UP", driver);
			help.threadWait(2000);
		}
		
		driver.findElement(textSearchButton).click();
		driver.findElement(textData).sendKeys("Caravan");
		driver.pressKeyCode(AndroidKeyCode.ENTER);
		help.threadWait(3);
		for (int i = 0; i < 20; i++) {
			help.threadWait(4000);
			help.scroll("UP", driver);
			help.scroll("UP", driver);
			help.scroll("UP", driver);
			help.threadWait(2000);
		}
		help.scroll("DOWN", driver);
		driver.findElement(searchAfterScroll).click();
		driver.findElement(textData).clear();
		
	}
	public void settingApp() {

		help.clickOnDataUsages(driver);
		help.findAppInSetting(driver,appilcationname);
		help.dataCollection(driver);

	}
	
	
}
