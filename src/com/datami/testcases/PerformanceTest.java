package com.datami.testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.datami.util.Helper;
import com.datami.util.ReadText;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;

public class PerformanceTest {
	AndroidDriver driver;
	By searchBox = By.xpath("//android.widget.EditText[contains(@resource-id,'acr.browser.lightning:id/search')]");
	By searchText = By.xpath("//android.widget.EditText[contains(@resource-id,'search_input')]");
	By searchButton = By.xpath("//android.widget.Button[contains(@resource-id,'search_submit')]");
	By newTab = By.xpath("//android.widget.TextView[contains(@resource-id,'browser.lightning:id/text1')]");
	By sandwich = By.xpath("//android.widget.TextView[contains(@resource-id,'qslc')]");
	By alert = By.xpath("//android.widget.TextView[contains(@resource-id,'android:id/message')]");
	By ok = By.xpath("//android.widget.Button[contains(@resource-id,'android:id/button1')]");

	By Error = By.xpath("//android.view.View[conatins(@text,'net::ERR_INTERNET_DISCONNECTED')]");

	// SettingApp
	By datausages = By.xpath("//android.widget.TextView[@text='Data usage']");
	By appilcationname = By.xpath("//android.widget.TextView[@text='Lightning']");
	By cellularServiceProvider = By.name("Vodafone IN");

	Helper help = new Helper();
	ReadText read = new ReadText();

	public PerformanceTest(AndroidDriver driver) {
		this.driver = driver;
	}

	public void wifiOff() {
		help.wifiControl(driver, "SAM", "OFF");
	}

	public void sdkPopup() {

		long startTime = System.currentTimeMillis();
		// help.wifiControl(driver, "SAM", "OFF");
		// System.out.println(driver.findElement(alert).getText());
		driver.findElement(ok).click();
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println("****************************" + totalTime + "****************************");
	}

	public void reset() {
		driver.removeApp("acr.browser.lightning");
		driver.installApp("./Application/LighteningBrowser-debug_2.0.4-QA.apk");
		// driver.resetApp();
		help.activitySwitch(driver, "acr.browser.lightning", "acr.browser.lightning.MainActivity");
		help.threadWait(3000);
	}

	public void notification() {
		try {
			help.threadWait(10000);
			help.notificationHandle(driver, "It’s on us! This app does not use your data.");

		} catch (Exception e) {
			e.getLocalizedMessage();
		}
	}

	public void browseData() {
		try {
			help.threadWait(4000);

			driver.findElement(searchText).sendKeys("HD Images");
			driver.findElement(searchButton).click();
		} catch (Exception e) {
			e.getLocalizedMessage();
		}
	}

	public void browseOnSecondTab() {

		help.swipe(driver, "Right");
		help.threadWait(4000);
		driver.findElement(newTab).click();
		help.threadWait(4000);
		driver.findElement(searchBox).sendKeys("https://www.nationalgeographic.com/");
		driver.pressKeyCode(AndroidKeyCode.ENTER);
		help.threadWait(4000);
		help.swipe(driver, "Right");
		help.threadWait(4000);
		driver.findElement(newTab).click();
		help.threadWait(4000);
		driver.findElement(searchBox).sendKeys("https://www.discovery.com//");
		driver.pressKeyCode(AndroidKeyCode.ENTER);
		help.threadWait(4000);
		// driver.findElement(sandwich).click();

	}

	public void wifiSearch() {

		System.out.println("In the application");
		help.wifiControl(driver, "SAM", "ON");
		help.screenShots(driver, "Wifi is On");
		help.threadWait(4000);
		help.swipe(driver, "Right");
		help.threadWait(4000);
		driver.findElement(newTab).click();
		help.threadWait(4000);
		driver.findElement(searchText).sendKeys("HD Images");
		driver.findElement(searchButton).click();
		help.threadWait(2000);
	}

	public void dataProvider() {

		driver.findElement(cellularServiceProvider).click();

	}

	public void dataUsed() {
		help.clickOnDataUsages(driver);
		help.scrollAndClick(driver, appilcationname);
		help.threadWait(2000);
		help.dataCollection(driver);
	//	read.dataInDas();
	}

}
