package com.datami.testcases;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.Reporter;
import org.testng.annotations.Parameters;
import org.testng.asserts.Assertion;
import org.testng.asserts.SoftAssert;

import com.datami.util.ExcelUtil;
import com.datami.util.Helper;
import com.datami.util.ReadText;

import io.appium.java_client.android.Activity;
//import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.android.Connection;

public class LightningApp {

	// ExcelUtil excel = new
	// ExcelUtil("//Users//rajeevmishra//Downloads//background_app_consumption.xlsx");

	AndroidDriver driver;
	By searchBox = By.xpath("//android.widget.EditText[contains(@resource-id,'acr.browser.lightning:id/search')]");
	By searchText = By.xpath("//android.widget.EditText[contains(@resource-id,'search_input')]");
	By searchButton = By.xpath("//android.widget.Button[contains(@resource-id,'search_submit')]");
	By newTab = By.xpath("//android.widget.TextView[contains(@resource-id,'browser.lightning:id/text1')]");
	By sandwich = By.xpath("//android.widget.TextView[contains(@resource-id,'qslc')]");
	By alert = By.xpath("//android.widget.TextView[contains(@resource-id,'android:id/message')]");
	By ok = By.xpath("//android.widget.Button[contains(@resource-id,'android:id/button1')]");
	By Error = By.xpath("//android.view.View[contains(@text,'net::ERR_INTERNET_DISCONNECTED')]");
	By ErrorClass = By.className("//android.webkit.WebView[contains(@content-desc,'Web page not available']");

	// SettingApp
	By datausages = By.xpath("//android.widget.TextView[@text='Data usage']");
	By appilcationname = By.xpath("//android.widget.TextView[@text='Lightning']");
	By cellularServiceProvider = By.name("Vodafone IN");

	Helper help = new Helper();
	ReadText read = new ReadText();

	public LightningApp(AndroidDriver driver) {
		this.driver = driver;
	}

	public void sdkPopup() {
		String file = ("./Application//" + "app-debug.apk");
		driver.installApp(file);
		help.wifiApp(driver, "OFF");
		
		help.implictWait(driver, 20);
		
		Reporter.log("SDK POP UP Message"+driver.findElement(alert).getText());
		driver.findElement(ok).click();
		
	}

	public void dualSimSupport(String service) {
		help.dualSimSupport(driver, service);
//		By alert = By.xpath("//android.widget.TextView[contains(@resource-id,'android:id/message')]");
//		System.out.println(driver.findElement(alert).getText());
//		Reporter.log(driver.findElement(alert).getText());
//		driver.findElement(ok).click();
//		help.browseData(driver, "www.datami.com");
	}

	public void flightModeBrowser() {

		help.flightMode2(driver, "ON");
		help.reset(driver);
		help.browseData(driver, "www.datami.com");
		help.wifiApp(driver, "ON");
		help.browseData(driver, "www.datami.com");
		help.flightMode2(driver, "OFF");

	}

	public void notification() {
		try {
			help.threadWait(5000);
			help.notificationHandle(driver, "It’s on us! This app does not use your data.");

		} catch (Exception e) {
			e.getLocalizedMessage();
		}
	}

	public void browseData() {

		Boolean s = help.browseData(driver, "www.datami.com");
	}

	public void browseOnSecondTab() {
		help.threadWait(2000);
		help.swipe(driver, "Right");
		help.threadWait(4000);
		driver.findElement(newTab).click();
		help.threadWait(4000);
		help.browseData(driver, "www.state.com");
		help.threadWait(4000);
		help.swipe(driver, "Right");
		help.threadWait(4000);
		driver.findElement(newTab).click();
		help.threadWait(4000);
		help.browseData(driver, "https://www.nature.com");
		driver.pressKeyCode(AndroidKeyCode.ENTER);
		help.threadWait(4000);
	}

	public void wifiSearch() {
		System.out.println("In the application");
		help.wifiApp(driver, "ON");
		help.screenShots(driver, "Wifi is On");
		help.threadWait(4000);
		help.swipe(driver, "Right");
		help.threadWait(4000);
		driver.findElement(newTab).click();
		help.threadWait(4000);
		driver.findElement(searchText).sendKeys("HD Images");
		driver.findElement(searchButton).click();
		help.threadWait(2000);
		help.wifiApp(driver, "OFF");
	}

	public void dataProvider() {
		driver.findElement(cellularServiceProvider).click();
	}
	public float dataUsed(String deviceId,String pkgName) {
		help.clickOnDataUsages(driver);
		help.scrollAndClick(driver, appilcationname);
		help.threadWait(2000);
		return	(help.dataCollection(driver));
		
	}
	public Float dataInDas(String deviceId,String pkgName) {
		Float a=read.dataInDas(deviceId, pkgName);
		return a;
	}
}
